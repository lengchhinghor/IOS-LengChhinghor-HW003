//
//  ViewController.swift
//  AppNotedCoreData
//
//  Created by Chhinghor's MacBook Pro on 7/11/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myTableList: UITableView!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var notes: [MyDataList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTableList.delegate = self
        myTableList.dataSource = self
        fetchNotes()
        changeLocal()
        // Do any additional setup after loading the view.
    }

//    func create note
    @IBAction func createNoted(_ sender: Any) {
        
        let addText = UIAlertController(title: "Note", message: "please create noted", preferredStyle: .alert)
        
        addText.addTextField{ text in text.placeholder =  "input note"}
        
        let creatAction = UIAlertAction(title: "Add", style: .default){
            _ in
            let noted = addText.textFields![0].text
            self.saveNoted(text: noted!)
            print("Save Data")
        }
        let cancelBtn  = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        addText.addAction(creatAction)
        addText.addAction(cancelBtn)
        
        present(addText, animated: true, completion: nil)
    }
    
    @IBAction func changeLanguage(_ sender: Any) {
        let popUpLang = UIAlertController(title: "Language", message: "Please select language", preferredStyle: .actionSheet)
        
        let khmerAction = UIAlertAction(title: "khmer", style: .default){ _ in
            AppService.shared.changeLang(lang: "km")
            self.title = "Note".localize()
        }
        
        let englishAction = UIAlertAction(title: "English", style: .default) { _ in
            //Change to English lang
            AppService.shared.changeLang(lang: "en")
            self.title = "Note".localize()
        }
        popUpLang.addAction(khmerAction)
        popUpLang.addAction(englishAction)
        present(popUpLang, animated: true, completion: nil)
    }
    
    
//    method for fetch data
    
    func fetchNotes(){
        let request = MyDataList.fetchRequest()
        do{
            self.notes = try context.fetch(request)
            self.myTableList.reloadData()
        }catch{
            print("Error")
        }
    }
    
    func delete (deleteNoted: MyDataList){
        context.delete(deleteNoted)
        try! context.save()
//        myTableList.deleteRows(at: <#T##[IndexPath]#>, with: <#T##UITableView.RowAnimation#>)
    }
    
    func changeLocal( ){
        self.title = "Note".localize()
    }
    
    
    // method save context
    func saveNoted(text: String){
        let noted = MyDataList(context: context)
        noted.title = text
        noted.id = UUID()
        noted.date = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long
        
        let timeString = formatter.string(from: noted.date!)

        fetchNotes()
        try? context.save()
        
    }
    @IBAction func clearAllNotes(_ sender: Any) {
        for note in notes {
            context.delete(note)
        }
        self.notes.removeAll()
        self.myTableList.reloadData()
        
        try? context.save()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = notes[indexPath.row].title
       
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "delete"){
            context, view, completion in
            
            let dNote = self.notes[indexPath.row]
            self.delete(deleteNoted: dNote)
            self.notes.remove(at: indexPath.row)
            self.myTableList.deleteRows(at: [indexPath], with: .automatic)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
}

extension String {
    func localize()-> String {
        
        let path = Bundle.main.path(forResource: AppService.shared.language, ofType: "lproj")
        
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: self, comment: self)
    }
}


struct AppService {
    
    static let shared = AppService()
    
    var language: String {
        return UserDefaults.standard.string(forKey: "lang") ?? "en"
    }
    
    func changeLang(lang: String){
        UserDefaults.standard.set(lang, forKey: "lang")
    }
}
