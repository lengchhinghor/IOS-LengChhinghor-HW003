//
//  CoreDataControl.swift
//  AppNotedCoreData
//
//  Created by Chhinghor's MacBook Pro on 7/11/21.
//

import Foundation
import CoreData

class CoreDataControl {
    
    static let shared = CoreDataControl(modelname: "AppNotedCoredData")
    
    let persistentContainer: NSPersistentContainer
    var ViewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    init(modelname: String){
        persistentContainer = NSPersistentContainer(name: modelname)
    }
    
    func loadCoreData(completion: (() -> Void)? = nil) {
        persistentContainer.loadPersistentStores{
            (description, error) in
            guard error == nil else {
                fatalError(error!.localizedDescription )
            }
            completion?()
        }
    }
}
