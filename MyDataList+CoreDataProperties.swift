//
//  MyDataList+CoreDataProperties.swift
//  AppNotedCoreData
//
//  Created by Chhinghor's MacBook Pro on 7/11/21.
//
//

import Foundation
import CoreData


extension MyDataList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MyDataList> {
        return NSFetchRequest<MyDataList>(entityName: "MyDataList")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var title: String?
    @NSManaged public var date: Date?

}

extension MyDataList : Identifiable {

}
